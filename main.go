package main

import (
	"flag"
	"net/http"
	"strconv"

	"github.com/lbc/fizzbuzz"
)

const DefaultPort = 8080

func main() {
	fbm := fizzbuzz.NewFizzBuzzManager()
	port := flag.Int("port", DefaultPort, "Port number")
	flag.Parse()

	portString := strconv.Itoa(*port)

	http.HandleFunc("/", fbm.Start)
	if err := http.ListenAndServe(":"+portString, nil); err != nil {
		panic(err)
	}
}
