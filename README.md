# FizzBuzz API

FizzBuzz api implementation using golang



# Usage

run :

./lbc -port=A_PORT 

if no port is specified the default port will be 8080

Then you can interact with the api in this manner :
goto : http://localhost:PORT/?string(n)=n&int(n)=Y&limit=X

# exemple :
http://localhost:8080/?string1=fizz&string2=buzz&string3=bazz&int1=3&int2=5&int3=8&limit=25

Every string has to have it's int (if you have a string42 you need a int42)

The the MAX_LIMIT value is set at 1000
