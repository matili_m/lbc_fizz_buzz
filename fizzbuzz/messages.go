package fizzbuzz

import (
	"encoding/json"
	"net/http"
)

func (fb *FizzBuzz) addError(resp *response, err error) {
	resp.Err = append(resp.Err, err.Error())
}

func (fb *FizzBuzz) addPanic(resp *response, err error) {
	resp.Panic = err.Error()
}

func (fb *FizzBuzz) create_response() []byte {
	message, _ := json.Marshal(fb.resp)
	return message
}

func (fb *FizzBuzz) send(w http.ResponseWriter, message []byte) {
	w.Write(message)
}
