package fizzbuzz

import (
	"bytes"
	"net/http"
	"strconv"
)

type response struct {
	Message string   `json:",omitempty"`
	Panic   string   `json:",omitempty"`
	Err     []string `json:",omitempty"`
	Warning []string `json:",omitempty"`
}

type FizzBuzz struct {
	limit int
	pairs map[string]int
	resp  *response
}

var (
	MAX_LIMIT = 1000
)

func (fb *FizzBuzz) getNumberResult(keys []string, number int, result *bytes.Buffer) {
	var i int

	for _, key := range keys {
		pv, _ := fb.pairs[key]
		if number%pv == 0 {
			result.WriteString(key)
			i = i + len(key)
		}
	}

	if i == 0 {
		result.WriteString(strconv.Itoa(number))
	}
}

func (fb *FizzBuzz) Start(w http.ResponseWriter, r *http.Request) {
	fb.parse(r.URL)
	if len(fb.resp.Panic) != 0 {
		message := fb.create_response()
		fb.send(w, message)
		return
	}

	result := fb.exec()
	fb.resp.Message = result.String()
	message := fb.create_response()
	fb.send(w, message)
}

func (fb *FizzBuzz) exec() bytes.Buffer {
	var result bytes.Buffer
	i := 0
	keys := make([]string, len(fb.pairs))

	for key := range fb.pairs {
		keys[i] = key
		i++
	}

	for i = 1; i <= fb.limit; i++ {
		if i != 1 {
			result.WriteString(",")
		}
		fb.getNumberResult(keys, i, &result)
	}

	return result
}
