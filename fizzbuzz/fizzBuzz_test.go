package fizzbuzz

import (
	"encoding/json"
	"io/ioutil"
	"net/http/httptest"
	"strconv"
	"testing"
)

func Test_RightResult(t *testing.T) {
	fbm := NewFizzBuzzManager()

	req := httptest.NewRequest("GET", "http://localhost:8080/?string1=fizz&string2=buzz&string3=bazz&int1=3&int2=5&int3=7&limit=15", nil)
	w := httptest.NewRecorder()

	fbm.Start(w, req)

	resp := w.Result()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal("Could read body ", err.Error())
	}

	expectedResp := &response{
		Message: "1,2,fizz,4,buzz,fizz,bazz,8,fizz,buzz,11,fizz,13,bazz,fizzbuzz",
	}

	rawExpectedResp, _ := json.Marshal(expectedResp)
	if string(data) != string(rawExpectedResp) {
		t.Fatal("Got wrong answer, want ", string(rawExpectedResp), "got ", string(data))
	}
}

func Test_MaxLimit(t *testing.T) {
	fbm := NewFizzBuzzManager()

	maxLimitString := strconv.Itoa(MAX_LIMIT + 1)
	req := httptest.NewRequest("GET", "http://localhost:8080/?string1=fizz&string2=buzz&string3=bazz&int1=3&int2=5&int3=7&limit="+maxLimitString, nil)
	w := httptest.NewRecorder()

	fbm.Start(w, req)

	resp := w.Result()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal("Could read body ", err.Error())
	}

	expectedResp := &response{
		Panic: "limit should be between 0 and " + strconv.Itoa(MAX_LIMIT),
	}

	rawExpectedResp, _ := json.Marshal(expectedResp)
	if string(data) != string(rawExpectedResp) {
		t.Fatal("Got wrong answer, want ", string(rawExpectedResp), "got ", string(data))
	}
}
func TestLimitPresent(t *testing.T) {
	fbm := NewFizzBuzzManager()

	req := httptest.NewRequest("GET", "http://localhost:8080/?string1=fizz&string2=buzz&string3=bazz&int1=3&int2=5&int3=7", nil)
	w := httptest.NewRecorder()

	fbm.Start(w, req)

	resp := w.Result()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal("Could read body ", err.Error())
	}

	expectedResp := &response{
		Panic: "limit param is not set",
	}

	rawExpectedResp, _ := json.Marshal(expectedResp)
	if string(data) != string(rawExpectedResp) {
		t.Fatal("Got wrong answer, want ", string(rawExpectedResp), "got ", string(data))
	}
}

func TestWrongPairs(t *testing.T) {
	fbm := NewFizzBuzzManager()

	req := httptest.NewRequest("GET", "http://localhost:8080/?string1=fizz&string2=buzz&int1=3&limit=15", nil)
	w := httptest.NewRecorder()

	fbm.Start(w, req)

	resp := w.Result()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal("Could read body ", err.Error())
	}

	expectedResp := &response{
		Message: "1,2,fizz,4,5,fizz,7,8,fizz,10,11,fizz,13,14,fizz",
		Err:     []string{"Parameter string2, has no pair parameter (int2)"},
	}

	rawExpectedResp, _ := json.Marshal(expectedResp)
	if string(data) != string(rawExpectedResp) {
		t.Fatal("Got wrong answer, want ", string(rawExpectedResp), "got ", string(data))
	}
}

func TestWrongValues(t *testing.T) {
	fbm := NewFizzBuzzManager()

	req := httptest.NewRequest("GET", "http://localhost:8080/?string1=fizz&string2=buzz&string3=bazz&int1=-1&int2=5&int3=7&limit=15", nil)
	w := httptest.NewRecorder()

	fbm.Start(w, req)

	resp := w.Result()
	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal("Could read body ", err.Error())
	}

	expectedResp := &response{
		Message: "1,2,3,4,buzz,6,bazz,8,9,buzz,11,12,13,bazz,buzz",
		Err:     []string{"int parameters should be above 0 int1 is not"},
	}

	rawExpectedResp, _ := json.Marshal(expectedResp)
	if string(data) != string(rawExpectedResp) {
		t.Fatal("Got wrong answer, want ", string(rawExpectedResp), "got ", string(data))
	}
}
