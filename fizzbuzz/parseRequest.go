package fizzbuzz

import (
	"errors"
	"fmt"
	"net/url"
	"strconv"
	"strings"
)

func (fb *FizzBuzz) parse(requestUrl *url.URL) {
	queries := requestUrl.Query()
	var err error

	fb.limit, err = fb.checkLimit(queries)
	if err != nil {
		fb.addPanic(fb.resp, err)
		return
	}
	for key, value := range queries {
		pv, ok, err := fb.findPair(key, queries)
		if err != nil {
			fb.addError(fb.resp, err)
			continue
		}
		if !ok {
			continue
		}

		if err := fb.checkValue(value); err != nil {
			fb.addError(fb.resp, err)
			continue
		}
		fb.pairs[value[0]] = pv
	}
}

func (fb *FizzBuzz) findPair(key string, queries url.Values) (int, bool, error) {
	if !strings.HasPrefix(key, "string") && !strings.HasPrefix(key, "int") && !strings.HasPrefix(key, "limit") {
		return 0, false, errors.New(fmt.Sprintf("Wrong parameter %v, the only ones supported are string[n] int[n] ex : string1 int1", key))
	}
	if !strings.HasPrefix(key, "string") {
		return 0, false, nil
	}

	idString := key[len("string"):]
	_, err := strconv.Atoi(idString)

	if err != nil {
		return 0, false, errors.New(fmt.Sprintf("Wrong parameter %v, the only ones supported are string[n] int[n] ex : string1=fizz int1=3", key))
	}

	pairValueString := queries.Get("int" + idString)
	if pairValueString == "" {
		return 0, false, errors.New(fmt.Sprintf("Parameter %v, has no pair parameter (int%v)", key, idString))
	}

	pairValue, _ := strconv.Atoi(pairValueString)
	if pairValue <= 0 {
		return 0, false, errors.New(fmt.Sprintf("int parameters should be above 0 %v is not", "int"+idString))
	}

	return pairValue, true, nil
}

func (fb *FizzBuzz) checkValue(value []string) error {
	if len(value) != 1 {
		return errors.New(fmt.Sprintf("Only want one value got %v %v times)", value, len(value)))
	}
	return nil
}

func (fb *FizzBuzz) checkLimit(queries url.Values) (int, error) {
	limitString := queries.Get("limit")
	if limitString == "" {
		return 0, errors.New("limit param is not set")
	}

	limit, err := strconv.Atoi(limitString)
	if err != nil {
		return 0, errors.New(fmt.Sprintf("wrong limit parameter, should be a number got : %v", limitString))
	}
	if limit > MAX_LIMIT || limit < 1 {
		return 0, errors.New(fmt.Sprintf("limit should be between 0 and %v", MAX_LIMIT))
	}

	return limit, nil
}
