package fizzbuzz

import "net/http"

type FizzBuzzManager struct{}

func NewFizzBuzzManager() *FizzBuzzManager {
	return &FizzBuzzManager{}
}

func (fbm *FizzBuzzManager) newFizzBuzz() *FizzBuzz {
	return &FizzBuzz{
		resp:  &response{},
		pairs: make(map[string]int),
	}
}

func (fbm *FizzBuzzManager) Start(w http.ResponseWriter, r *http.Request) {
	fb := fbm.newFizzBuzz()

	fb.Start(w, r)
}
